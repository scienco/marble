import { app, BrowserWindow, ipcMain } from "electron";
import * as path from "path";
import * as url from "url";
import * as fs from "fs";

const isDevelopment = process.env.NODE_ENV === 'development';
const appUrls = {
  development: 'http://localhost:4200',
  production: url.format({
      pathname: path.join(__dirname, `/marble-app/index.html`),
      protocol: "file:",
      slashes: true
    })
};
let win: BrowserWindow;

app.on("ready", createWindow);

app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
});

function createWindow() {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: { nodeIntegration: true }
  });

  win.loadURL(isDevelopment ? appUrls.development : appUrls.production);

  if (isDevelopment || true) {
    win.webContents.openDevTools();
  }

  win.on("closed", () => {
    win = null;
  });
}

ipcMain.on("getFiles", (event, arg) => {
  const files = fs.readdirSync(__dirname);
  win.webContents.send("getFilesResponse", files);
});